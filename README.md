# cloudtalk


#### 软件架构
![架构草图](https://images.gitee.com/uploads/images/2019/0103/094012_5bf53aa1_494527.png "imnew.png")


#### App截图

![会话界面](https://images.gitee.com/uploads/images/2019/0103/093049_537ceecd_494527.png "screenshot_20190103_092934.png")

![通讯录](https://images.gitee.com/uploads/images/2019/0103/093128_023ac1a6_494527.png "screenshot_20190103_092726.png")

![发现](https://images.gitee.com/uploads/images/2019/0103/093157_7dcf2acd_494527.png "screenshot_20190103_092734.png")

![群列表](https://images.gitee.com/uploads/images/2019/0103/093232_d8826898_494527.png "screenshot_20190103_092750.png")

![新的朋友](https://images.gitee.com/uploads/images/2019/0103/093254_82a327e5_494527.png "screenshot_20190103_092759.png")

![聊天页面](https://images.gitee.com/uploads/images/2019/0103/093317_808871da_494527.png "screenshot_20190103_092906.png")

![云表情页面](https://images.gitee.com/uploads/images/2019/0103/093338_c5da6e8e_494527.png "screenshot_20190103_092845.png")

![发送位置](https://images.gitee.com/uploads/images/2019/0103/093359_3cc12a22_494527.png "screenshot_20190103_092827.png")


![音视频通话](https://images.gitee.com/uploads/images/2019/0103/093720_f3fd5f0c_494527.png "screenshot_20190103_092923.png")



感谢以下开源项目提供强有力的支持:

[t-io 不仅仅百万并发框架](https://gitee.com/tywo45/t-io)  cloudtalk 中的 websocket 模块基于tio开发，为 h5/wss 提供强有力的支持!

[ J-IM](https://gitee.com/xchao/j-im) 非常不错的基于tio的即时通讯服务端!
